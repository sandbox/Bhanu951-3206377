# Countries List

A Module to provide countries list.

Source JSON Data is available at https://github.com/dr5hn/countries-states-cities-database

https://github.com/localgovdrupal/localgov_geo/blob/1.x/tests/src/Functional/GeoBundleCreationTest.php


## Test Case Scenarios :

1. Enable Module load home page as annon and check access.

2. Access home page as auth user.

3. Access home page as admin (not root user).

4. Access admin page as annon user.

5. Access admin page as auth user.

6. Access admin page as admin (not root user).

7. Test Module reinstall.

8. Test countries list content page access.

9. Test countries list content creation.

10. Test article creation.

11. Test countries list and article deletion.

12. Test Migration process.

13. Test Migration results.

## Approach :

Create base abstract class and extend it.

a. Test case 1 in one class.

b. Test case 2 and 3 in one class.

c. Test case 4,5,6 in one class.

d. Test case 7 in one class.

e. Test case 8 and 9 in one class.

## Issues :

Why isn't the session is being carry forwarded from one class to another.
Say for example I am logged in as admin in one class and
the same user session is not able to be carry forwarded to another class.

Why is the new user being created in each new method.

Order of tests execution.

Config not being imported on module install.

