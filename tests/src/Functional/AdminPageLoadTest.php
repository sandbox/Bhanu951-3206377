<?php

namespace Drupal\Tests\countries_list\Functional;

/**
 * Class to test site load admin page after module enabled.
 *
 * @group countries_list
 */
class AdminPageLoadTest extends CountriesListFunctionalTestBase {

  /**
   * Tests the admin page response.
   */
  public function testAdminConfigPage() {

    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/config');
    // Make sure we don't get a 403 code.
    $this->assertSession()->statusCodeEquals(200);

    // Check for preconfigured languages.
    $this->drupalGet('admin/config/regional/language');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('English');
    $this->assertSession()->pageTextContains('German');
    $this->assertSession()->pageTextContains('Telugu');

    // Logout admin user, try to access the page as normal user.
    $this->drupalLogout();

    // Login as Normal user.
    $this->drupalLogin($this->normalUser);
    $this->drupalGet('/admin/config');
    // Make sure we don't get a 200 code.
    $this->assertSession()->statusCodeEquals(403);

  }

}
