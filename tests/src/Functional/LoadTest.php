<?php

namespace Drupal\Tests\countries_list\Functional;

/**
 * Class to test site load after module enabled.
 *
 * @group countries_list
 */
class LoadTest extends CountriesListFunctionalTestBase {

  /**
   * Tests the home page loads for Anonymous users.
   */
  public function testHomePage() {

    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', 'config:block_list');
    // Minimal homepage title.
    // $this->assertSession()->pageTextContains('Log in');.
    $this->assertSession()->pageTextContains('Welcome to Drupal');
  }

  /**
   * Test User Login.
   */
  public function testUserLogin() {

    $this->drupalLogin($this->adminUser);
    $this->assertSession()->pageTextContains($this->adminUser->getDisplayName());
    $this->drupalGet('/admin');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalLogout();

  }

  /**
   * Test Block Placement.
   */
  public function testBlockPlacement() {

    // Log in as a content Editor.
    $this->drupalLogin($this->contentEditor);

    // Place the powered by block.
    $this->drupalPlaceBlock('system_powered_by_block', ['id' => 'powered_by']);

    // Visit the frontpage. We should see the block.
    $this->drupalGet('<front>');
    $this->assertSession()->pageTextContains('Powered by Drupal');
    $this->drupalLogout();

  }

}
