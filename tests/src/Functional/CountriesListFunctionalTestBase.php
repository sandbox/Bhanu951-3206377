<?php

namespace Drupal\Tests\countries_list\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Base test class for Countries List functional tests.
 *
 * @group countries_list
 */
abstract class CountriesListFunctionalTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $defaultProfile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // Modules for core functionality.
    'node',
    'views',
    'system',
    'telephone',
    'language',
    'user',
    'config',
    'token',
    'pathauto',
    'taxonomy',
    'admin_toolbar',
    'migrate_plus',
    'migrate_file',
    'module_filter',
    'menu_ui',
    'block',
    'paragraphs',
    'geofield',
    'content_moderation',
    'content_translation',
    'countries_list',
  ];

  /**
   * A user with permissions for site administration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * A user with permissions to configure blocks.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $contentEditor;

  /**
   * A user with minimal permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $normalUser;

  /**
   * A user with permissions to publish countries list content.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $cLContentEditor;

  /**
   * The permissions of the admin user.
   *
   * @var string[]
   */
  protected $adminUserPermissions = [
    'access administration pages',
    'administer site configuration',
    'administer countries list configuration',
    'administer modules',
    'administer nodes',
    'administer content types',
    'view any unpublished content',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {

    // Make sure to complete the normal setup steps first.
    parent::setUp();

    // Set the front page to "/node".
    $this->config('system.site')
      ->set('page.front', '/node')
      ->save(TRUE);

    $this->adminUser = $this->drupalCreateUser($this->adminUserPermissions);

    $this->normalUser = $this->drupalCreateUser([]);

    $this->contentEditor = $this->drupalCreateUser(['administer blocks']);

    $this->cLContentEditor = $this->drupalCreateUser(['administer countries list configuration']);

  }

}
