<?php

namespace Drupal\Tests\countries_list\Functional;

/**
 * Class to test site load after module enabled.
 *
 * @group countries_list
 */
class ModuleUninstallTest extends CountriesListFunctionalTestBase {

  /**
   * Tests the module unistall.
   */
  public function testModuleUninstall() {

    // Uninstall the module.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/modules/uninstall');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Countries List');
    $this->submitForm(['uninstall[countries_list]' => TRUE], 'Uninstall');
    $this->assertSession()->pageTextContains('node.field_country_capital');
    $this->assertSession()->pageTextContains('node.field_country_coordinates');
    $this->assertSession()->pageTextContains('node.field_country_currency');
    $this->assertSession()->pageTextContains('node.field_country_currency_symbol');
    $this->assertSession()->pageTextContains('node.field_country_id');
    $this->assertSession()->pageTextContains('node.field_country_iso2');
    $this->assertSession()->pageTextContains('node.field_country_iso3');
    $this->assertSession()->pageTextContains('node.field_country_name_translations');
    $this->assertSession()->pageTextContains('node.field_country_native');
    $this->assertSession()->pageTextContains('node.field_country_phone_code');
    $this->assertSession()->pageTextContains('node.field_country_region');
    $this->assertSession()->pageTextContains('node.field_country_region_taxonomy');
    $this->assertSession()->pageTextContains('node.field_country_subregion');
    $this->assertSession()->pageTextContains('node.field_country_subregion_taxonomy');
    $this->assertSession()->pageTextContains('node.field_country_time_zones');
    $this->assertSession()->pageTextContains('paragraph.field_country_abbreviation');
    $this->assertSession()->pageTextContains('paragraph.field_country_gmt_offset');
    $this->assertSession()->pageTextContains('paragraph.field_country_gmt_offset_name');
    $this->assertSession()->pageTextContains('paragraph.field_country_name_translations');
    $this->assertSession()->pageTextContains('paragraph.field_country_tz_name');
    $this->assertSession()->pageTextContains('paragraph.field_country_zone_name');
    $this->assertSession()->pageTextContains('Country Name Translations');
    $this->assertSession()->pageTextContains('Country Time Zones');
    $this->assertSession()->pageTextContains('Country Region Taxonomy');
    $this->assertSession()->pageTextContains('Country Region');
    $this->assertSession()->pageTextContains('Country Sub Region');
    $this->assertSession()->pageTextContains('Country Name Translations');
    $this->assertSession()->pageTextContains('Country Capital');
    $this->assertSession()->pageTextContains('Country Coordinates');
    $this->assertSession()->pageTextContains('Country Currency');
    $this->assertSession()->pageTextContains('Country Currency Symbol');
    $this->assertSession()->pageTextContains('Country ID');
    $this->assertSession()->pageTextContains('Country ISO2');
    $this->assertSession()->pageTextContains('Country ISO3');
    $this->assertSession()->pageTextContains('Country Name Translations');
    $this->assertSession()->pageTextContains('Country Native');
    $this->assertSession()->pageTextContains('Country Phone Code');
    $this->assertSession()->pageTextContains('Country Region');
    $this->assertSession()->pageTextContains('Country Region Taxonomy');
    $this->assertSession()->pageTextContains('Country Sub Region');
    $this->assertSession()->pageTextContains('Country Sub Region Taxonomy');
    $this->assertSession()->pageTextContains('Country Time Zones');
    $this->assertSession()->pageTextContains('Country Name Translations');
    $this->assertSession()->pageTextContains('Country Abbreviation');
    $this->assertSession()->pageTextContains('Country GMT Offset');
    $this->assertSession()->pageTextContains('Country GMT Offset Name');
    $this->assertSession()->pageTextContains('Country TZ Name');
    $this->assertSession()->pageTextContains('Country Zone Name');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()->pageTextContains('The selected modules have been uninstalled.');
    $this->assertSession()->pageTextNotContains('Countries List');
    // $this->drupalLogout();
  }

}
