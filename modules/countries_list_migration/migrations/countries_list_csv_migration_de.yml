id: countries_list_csv_migration_de
label: CSV Import of Countries List

migration_group: countries_list

migration_tags:
  - Countries List CSV Source

dependencies:
  enforced:
    module:
      - countries_list_migration
source:
  plugin: 'remote_csv_file_parser'
  # Full path to the file.
  # path: public://countries.csv
  path : http://d8cards.lndo.site/sites/default/files/migration/source/countries.csv
  # Column delimiter. Comma (,) by default.
  delimiter: ','
  # Field enclosure. Double quotation marks (") by default.
  enclosure: '"'
  # The row to be used as the CSV header (indexed from 0),
  # or null if there is no header row.
  header_offset: 0
  # The column(s) to use as a key. Each column specified will
  # create an index in the migration table and too many columns
  # may throw an index size error.
  ids:
    - id
  constants:
    AUTHOR_UID: 2
    REGION_TAXONOMY: country_region
    SUBREGION_TAXONOMY: country_subregion
    LANG_DE: 'de'
    BUNDLE_TYPE: countries_list

  # Here we identify the columns of interest in the source filed.

  fields:
    -
      name: id
      label: 'Unique Subs Id'
    -
      name: name
      label: 'Country Name'
    -
      name: iso3
      label: 'Country ISO3'
    -
      name: iso2
      label: 'Country ISO2'
    -
      name: phone_code
      label: 'Country Phone Code'
    -
      name: capital
      label: 'Country Capital'
    -
      name: currency
      label: 'Country Currency'
    -
      name: currency_symbol
      label: 'Country Currency Symbol'
    -
      name : tld
      label: 'Country TLD'
    -
      name: native
      label: 'Country Native'
    -
      name: region
      label: 'Country Region'
    -
      name: subregion
      label: 'Country Sub Region'
    -
      name: timezones
      label: 'Country Time Zones'
    -
      name: translations
      label: 'Country Name Translations'
    -
      name: latitude
      label: 'Country Latitude'
    -
      name: longitude
      label: 'Country Longitude'
    -
      name: emoji
      label: 'Country emoji'
    -
      name: emojiU
      label: 'Country emojiU'

process:

  type: constants/BUNDLE_TYPE

  langcode: constants/LANG_DE

  uid: constants/AUTHOR_UID

  status:
    plugin: default_value
    default_value: 1

  nid:
    -
      plugin: countries_list_name_translation
      source: id

  field_country_id: id

  title:
    -
      plugin: concat
      source:
        - name
        - constants/LANG_DE
      delimiter: '-'

  field_country_iso3:
    -
      plugin: default_value
      source: iso3
      default_value: 'NA'

  field_country_iso2:
    -
      plugin: default_value
      source: iso2
      default_value: 'NA'

  field_country_phone_code:
    -
      plugin: default_value
      source: phone_code
      default_value: 'NA'

  field_country_capital:
    -
      plugin: default_value
      source: capital
      default_value: 'NA'

  field_country_currency:
    -
      plugin: default_value
      source: currency
      default_value: 'NA'

  field_country_currency_symbol:
    -
      plugin: default_value
      source: currency_symbol
      default_value: 'NA'

  field_country_native:
    -
      plugin: default_value
      source: native
      default_value: 'NA'

  field_country_region:
    -
      plugin: default_value
      source: region
      default_value: 'NA'

  field_country_subregion:
    -
      plugin: default_value
      source: subregion
      default_value: 'NA'


  # This field is used to format input timezones data.
  pseudo_field_country_time_zones:
    -
      plugin: country_timezones_csv_data_formatter
      source: timezones

  # Paragraphs Field.
  field_country_time_zones:
   -
      plugin: country_timezones_paragraphs
      source: '@pseudo_field_country_time_zones'

  # This field is used to format input translations data.
  pseudo_field_country_name_translations:
    -
      plugin: country_name_translations_csv_data_formatter
      source: translations

  # Country Name Translations Paragraphs Field.
  field_country_name_translations:
    -
      plugin: country_name_translations_paragraphs
      source: '@pseudo_field_country_name_translations'

  # Country Coordinates.
  field_country_coordinates:
    -
      plugin: geofield_latlon
      source:
        - latitude
        - longitude

  # Country Region Taxonomy Term
  field_country_region_taxonomy:
    -
      plugin: countries_list_region_taxonomy
      source:
        region: region
        bundle: constants/REGION_TAXONOMY

  # Country Sub Region Taxonomy Term
  field_country_subregion_taxonomy:
    -
      plugin: countries_list_region_taxonomy
      source:
        region: subregion
        bundle: constants/SUBREGION_TAXONOMY

destination:
  plugin: entity:node
  translations: true

migration_dependencies:
  required: { }
  optional:
    - countries_list_csv_migration
