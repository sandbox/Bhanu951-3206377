id: countries_list_json_migration
label: JSON feed of Countries List

migration_group: countries_list

migration_tags:
  - Countries List JSON Source

dependencies:
  enforced:
    module:
      - countries_list_migration
source:
  # We use the JSON source plugin.
  plugin: url
  data_fetcher_plugin: http
  data_parser_plugin: countries_list_json

  urls:
    - https://raw.githubusercontent.com/dr5hn/countries-states-cities-database/master/countries.json
    # - https://gitlab.com/-/snippets/2066494/raw/master/countries.json
    # - https://gitlab.com/-/snippets/2066968/raw/master/countries_small.json

  # The data_parser normally limits the fields passed on to the source plugin
  # to fields configured to be used as part of the migration. To support more
  # dynamic migrations, the JSON data parser supports including the original
  # data for the current row. Simply include the 'include_raw_data' flag set
  # to `true` to enable this. This option is disabled by default to minimize
  # memory footprint for migrations that do not need this capability.
  include_raw_data: true

  item_selector: /

  fields:
    -
      name: field_country_id
      label: 'Country ID'
      selector: id
    -
      name: field_country_name
      label: 'Country Name'
      selector: name
    -
      name: field_country_iso3
      label: 'Country ISO3'
      selector: iso3
    -
      name: field_country_iso2
      label: 'Country ISO2'
      selector: iso2
    -
      name: field_country_phone_code
      label: 'Country Phone Code'
      selector: phone_code
    -
      name: field_country_capital
      label: 'Country Capital'
      selector: capital
    -
      name: field_country_currency
      label: 'Country Currency'
      selector: currency
    -
      name: field_country_currency_symbol
      label: 'Country Currency Symbol'
      selector: currency_symbol
    -
      name: field_country_native
      label: 'Country Native'
      selector: native
    -
      name: field_country_region
      label: 'Country Region'
      selector: region
    -
      name: field_country_subregion
      label: 'Country Sub Region'
      selector: subregion
    -
      name: field_country_time_zones
      label: 'Country Time Zones'
      selector: timezones
    -
      name: field_country_name_translations
      label: 'Country Name Translations'
      selector: translations
    -
      name: field_country_latitude
      label: 'Country Latitude'
      selector: latitude
    -
      name: field_country_longitude
      label: 'Country Longitude'
      selector: longitude


  # Under 'ids', we identify source fields populated above which will uniquely
  # identify each imported item. The 'type' makes sure the migration map table
  # uses the proper schema type for stored the IDs.
  ids:
    field_country_id:
      type: integer
  constants:
    AUTHOR_UID: 2
    REGION_TAXONOMY: country_region
    SUBREGION_TAXONOMY: country_subregion
    LANG_EN: 'en'
    BUNDLE_TYPE: countries_list

process:

  type: constants/BUNDLE_TYPE

  langcode: constants/LANG_EN

  uid: constants/AUTHOR_UID

  status:
    plugin: default_value
    default_value: 1

  field_country_id: field_country_id

  title: field_country_name

  field_country_iso3:
    -
      plugin: default_value
      source: field_country_iso3
      default_value: 'NA'

  field_country_iso2:
    -
      plugin: default_value
      source: field_country_iso2
      default_value: 'NA'

  field_country_phone_code:
    -
      plugin: default_value
      source: field_country_phone_code
      default_value: 'NA'

  field_country_capital:
    -
      plugin: default_value
      source: field_country_capital
      default_value: 'NA'

  field_country_currency:
    -
      plugin: default_value
      source: field_country_currency
      default_value: 'NA'

  field_country_currency_symbol:
    -
      plugin: default_value
      source: field_country_currency_symbol
      default_value: 'NA'

  field_country_native:
    -
      plugin: default_value
      source: field_country_native
      default_value: 'NA'

  field_country_region:
    -
      plugin: default_value
      source: field_country_region
      default_value: 'NA'

  field_country_subregion:
    -
      plugin: default_value
      source: field_country_subregion
      default_value: 'NA'

  # Country Time Zones Paragraphs Field.
  field_country_time_zones:
    -
      plugin: country_timezones_paragraphs
      source: field_country_time_zones

  # Country Name Translations Paragraphs Field.
  field_country_name_translations:
    -
      plugin: country_name_translations_paragraphs
      source: field_country_name_translations

  # Country Coordinates.
  field_country_coordinates:
    -
      plugin: geofield_latlon
      source:
        - field_country_latitude
        - field_country_longitude

  # Country Region Taxonomy Term
  field_country_region_taxonomy:
    -
      plugin: countries_list_region_taxonomy
      source:
        region: field_country_region
        bundle: constants/REGION_TAXONOMY

  # Country Sub Region Taxonomy Term
  field_country_subregion_taxonomy:
    -
      plugin: countries_list_region_taxonomy
      source:
        region: field_country_subregion
        bundle: constants/SUBREGION_TAXONOMY

destination:
  plugin: entity:node
  translations: false

migration_dependencies:
  required: { }
  optional: { }
