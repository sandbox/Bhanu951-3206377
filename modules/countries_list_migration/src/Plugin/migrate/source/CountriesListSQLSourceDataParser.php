<?php

namespace Drupal\countries_list_migration\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * The Countries List SQL Source Data Parser plugin.
 *
 * @MigrateSource(
 *   id = "countries_list_sql_source_data_parser",
 *   source_module = "countries_list_migration"
 * )
 */
class CountriesListSQLSourceDataParser extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('countries', 'c');
    $query->fields('c', ['id',
      'name', 'iso3', 'iso2', 'phonecode', 'capital',
      'currency', 'currency_symbol', 'native', 'region', 'subregion',
      'timezones', 'translations', 'latitude', 'longitude', 'emoji', 'emojiU',
    ]);
    $query->orderBy('c.id', 'DESC');
    $query->condition('c.id', [0, 251], 'BETWEEN');
    // $query->range(0, 250);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id' => $this->t('Country ID'),
      'name' => $this->t('Country Name'),
      'iso3' => $this->t('Country ISO3'),
      'iso2' => $this->t('Country ISO2'),
      'phonecode' => $this->t('Country Phone Code'),
      'capital' => $this->t('Country Capital'),
      'currency' => $this->t('Country Currency'),
      'currency_symbol' => $this->t('Country Currency Symbol'),
      'native' => $this->t('Country Native'),
      'region' => $this->t('Country Region'),
      'subregion' => $this->t('Country Sub Region'),
      'timezones' => $this->t('Country Time Zones'),
      'translations' => $this->t('Country Name Translations'),
      'latitude' => $this->t('Country Latitude'),
      'longitude' => $this->t('Country Longitude'),
      'emoji' => $this->t('Country emoji'),
      'emojiU' => $this->t('Country emojiU'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['id'] = [
      'type' => 'integer',
    ];
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    $time_zones_source = $row->getSourceProperty('timezones');
    $name_translations_source = $row->getSourceProperty('translations');
    $time_zones_formatted = json_decode($time_zones_source, TRUE);
    $name_translations_formatted = json_decode($name_translations_source, TRUE);
    $row->setSourceProperty('timezones', $time_zones_formatted);
    $row->setSourceProperty('translations', $name_translations_formatted);
    return parent::prepareRow($row);

  }

}
