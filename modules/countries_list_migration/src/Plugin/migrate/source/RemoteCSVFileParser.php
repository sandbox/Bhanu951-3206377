<?php

namespace Drupal\countries_list_migration\Plugin\migrate\source;

use Drupal\migrate_source_csv\Plugin\migrate\source\CSV as SourceCSV;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\MigrateException;

/**
 * Migration csv file from remote source.
 *
 * This source extends the CSV source introduced by the migrate_source_csv
 * module. It makes a requests to the provided remote url and downloads
 * the remote file to a temporary location and then pass the path to
 * the temporary file to the CSV plugin.
 *
 * @MigrateSource(
 *   id = "remote_csv_file_parser"
 * )
 */
class RemoteCSVFileParser extends SourceCSV {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    // Prepare connection parameters.
    if (!empty($configuration['path'])) {
      // We simply download the remote CSV file to a temporary path and set
      // the temporary path to the parent CSV plugin.
      $configuration['path'] = $this->downloadFile($configuration['path']);
    }
    // If the file downloaded successfully with SFTP, then the "path" parameter
    // will be populated and the parent plugin will detect the CSV file.
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this->httpClient = \Drupal::httpClient();
  }

  /**
   * Downloads a file from remote url.
   *
   * @param string $url
   *   Remote URL.
   *
   * @throws \Drupal\migrate\MigrateException
   *   If something goes wrong.
   *
   * @return string
   *   Path to local cached version of the remote file.
   */
  protected function downloadFile(string $url) {

    // Remote file path must be specified.
    // Without knowing the remote file path, we cannot download the file!
    if (empty($url)) {
      throw new MigrateException('Required parameter "path" not defined.');
    }

    $tmp_path = \Drupal::service('file_system')->getTempDirectory();

    $path_local = $tmp_path . '/countries.csv';

    \Drupal::httpClient()->get($url, ['save_to' => $path_local]);

    // Return path to the local of the file.
    // This will in turn be passed to the parent CSV plugin.
    return $path_local;
  }

}
