<?php

namespace Drupal\countries_list_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Provides a Country TimeZones CSV Data Formatter plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: country_timezones_csv_data_formatter
 *     source: source_timezones_data
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "country_timezones_csv_data_formatter",
 *   handle_multiples = TRUE
 * )
 */
class CountryTimeZonesCSVDataFormatter extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function multiple(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $cleaned_data = [];

    $cleaned_data = json_decode($value, TRUE);

    return $cleaned_data;
  }

}
