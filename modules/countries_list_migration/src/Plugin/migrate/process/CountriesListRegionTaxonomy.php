<?php

namespace Drupal\countries_list_migration\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a countries_list_region_taxonomy plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: countries_list_region_taxonomy
 *     source: source_field_name
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "countries_list_region_taxonomy",
 *   handle_multiples = TRUE
 * )
 */
class CountriesListRegionTaxonomy extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('countries_list_migration');
    $instance->languageManager = $container->get('language_manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $taxonomy = [];

    if (isset($value)) {
      $taxonomy[] = $this->createCountriesListRegionTaxonomy($value);
    }

    return $taxonomy;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple(): bool {
    return TRUE;
  }

  /**
   * Generates a taxonomy term entity.
   *
   * @param array $value
   *   Indivial Taxonomy Term.
   *
   * @return array
   *   Returns created Taxonomy Term Id.
   */
  protected function createCountriesListRegionTaxonomy(array $value) {

    $term = $value[0];

    $vid = $value[1];

    $language = $this->languageManager->getCurrentLanguage()->getId();

    if (!empty($term)) {

      $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
      $query->condition('vid', $vid);
      $query->condition('name', $term);
      $tid = $query->execute();

      if (empty($tid)) {

        $new_term = $this->entityTypeManager->getStorage('taxonomy_term')->create([
          'langcode' => $language,
          'name' => $term,
          'description' => [
            'value' => $term . ' Region',
            'format' => 'full_html',
          ],
          'vid' => $vid,
          'parent' => [],
        ]);

        // Enforcing it as new.
        $new_term->enforceIsNew();
        // Save the taxonomy term.
        $new_term->save();

        $target_id = $new_term->id();

        $string = str_replace(' ', '-', mb_strtolower($term));

        $path_alias = $this->entityTypeManager->getStorage('path_alias')->create([
          'path' => '/taxonomy/term/' . $new_term->id(),
          'alias' => '/region/' . $string,
          'langcode' => $language,
        ]);

        $path_alias->save();

      }
      else {
        foreach ($tid as $term_id) {
          $target_id = $term_id;
        }

      }

    }

    $this->logger->notice($this->t(
      'Term ID for @term is : @id',
      [
        '@id' => $target_id,
        '@term' => $term,
      ]
    ));

    // Return the taxonomy term id.
    return [
      'target_id' => $target_id,
    ];

  }

}
