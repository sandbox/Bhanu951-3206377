<?php

namespace Drupal\countries_list_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Provides a Country Name Translations CSV Data Formatter plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: country_name_translations_csv_data_formatter
 *     source: source_translations_data
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "country_name_translations_csv_data_formatter",
 *   handle_multiples = TRUE
 * )
 */
class CountryNameTranslationsCSVDataFormatter extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function multiple(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $formatted_data = [];

    $cleaned_data = (array) json_decode($value, TRUE);

    foreach ($cleaned_data as $key => $data) {
      $formatted_data[$key] = [
        'key' => $key,
        'value' => $data,
      ];
    }
    return $formatted_data;
  }

}
