<?php

namespace Drupal\countries_list_migration\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a countries_list_name_translation plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: countries_list_name_translation
 *     source: source_field_name
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "countries_list_name_translation"
 * )
 */
class CountriesListNameTranslation extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('countries_list_migration');
    $instance->languageManager = $container->get('language_manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $nid = [];
    // Indivial Country ID .
    if (isset($value)) {

      $storage = \Drupal::entityTypeManager()->getStorage('node');
      $query = $storage->getQuery()
        ->condition('type', 'countries_list')
        ->condition('status', Node::PUBLISHED)
        ->condition('langcode', 'EN')
        ->condition('field_country_id', $value, '=')
        ->execute();
    }

    foreach ($query as $data) {
      $nid = $data;
    }
    // Nid of the base translation.
    return $nid;
  }

}
