<?php

namespace Drupal\countries_list_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Provides a Country Name Translations XML Data Formatter plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: country_name_translations_xml_data_formatter
 *     source: source_field_name
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "country_name_translations_xml_data_formatter",
 *   handle_multiples = TRUE
 * )
 */
class CountryNameTranslationsXMLDataFormatter extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function multiple(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $formatted_data = [];

    $cleaned_data = (array) $value;

    foreach ($cleaned_data as $key => $data) {
      $formatted_data[$key] = [
        'key' => $key,
        'value' => $data,
      ];

    }

    return $formatted_data;
  }

}
