<?php

namespace Drupal\countries_list_migration\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Provides a country_name_translations migration plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: country_name_translations_paragraphs
 *     source: source_field_name
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "country_name_translations_paragraphs",
 *   handle_multiples = TRUE
 * )
 */
class CountryNameTranslationsParagraphs extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Constructs a CountriesTimezones plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger->get('countries_list_migration');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $paragraphs = [];

    foreach ($value as $item) {
      $paragraphs[] = $this->createCountryNameTranslationsParagraphItem($item);
    }

    return $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple(): bool {
    return TRUE;
  }

  /**
   * Generates a paragraph item entity.
   *
   * @param array $item
   *   Indivial Translation array.
   *
   * @return array
   *   Returns created paragraph.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\migrate\MigrateException
   */
  protected function createCountryNameTranslationsParagraphItem(array $item): array {

    $paragraph = Paragraph::create([

      'type' => 'country_name_translations',

      'field_country_name_translations' => [
        'value'  => $item['key'] . " : " . $item['value'],
      ],
    ]);

    $this->logger->notice(
      'The following language translation is imported: @translation',
      [
        '@translation' => $item['value'],
      ]
    );

    $paragraph->save();

    return [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
  }

}
