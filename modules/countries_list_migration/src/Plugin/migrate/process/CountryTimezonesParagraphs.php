<?php

namespace Drupal\countries_list_migration\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Provides a countries_timezones migration plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: country_timezones_paragraphs
 *     source: source_field_name
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "country_timezones_paragraphs",
 *   handle_multiples = TRUE
 * )
 */
class CountryTimezonesParagraphs extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Constructs a CountriesTimezones plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger->get('countries_list_migration');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    foreach ($value as $item) {
      $paragraphs[] = $this->createCountryTimezonesParagraphsItem($item);
    }

    return $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple(): bool {
    return TRUE;
  }

  /**
   * Generates a paragraph item entity.
   *
   * @param array $item
   *   Indivial timezone array.
   *
   * @return array
   *   Returns created paragraph.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\migrate\MigrateException
   */
  protected function createCountryTimezonesParagraphsItem(array $item): array {

    $paragraph = Paragraph::create([

      'type' => 'country_timezones',

      'field_country_abbreviation' => [
        'value'  => $item['abbreviation'],
      ],
      'field_country_gmt_offset' => [
        'value'  => $item['gmtOffset'],
      ],
      'field_country_gmt_offset_name' => [
        'value'  => $item['gmtOffsetName'],
      ],
      'field_country_tz_name' => [
        'value'  => $item['tzName'],
      ],
      'field_country_zone_name' => [
        'value'  => $item['zoneName'],
      ],
    ]);

    $this->logger->notice(
      'The following timezone Id is imported: @timezone_name',
          [
            '@timezone_name' => $item['zoneName'],
          ]
        );

    $paragraph->save();

    return [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];
  }

}
