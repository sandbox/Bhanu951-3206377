<?php

namespace Drupal\countries_list_migration\Plugin\migrate_plus\data_parser;

use Drupal\migrate_plus\Plugin\migrate_plus\data_parser\Json;

/**
 * Obtain JSON data for migration.
 *
 * Inspired from:
 *
 * https://www.nikunj.dev/migrating-multi-value-non-associative-data
 *
 * @DataParser(
 *   id = "countries_list_json",
 *   title = @Translation("Countries List JSON")
 * )
 */
class CountriesListJson extends Json {

  /**
   * {@inheritdoc}
   */
  protected function getSourceData($url) {

    $response = $this->getDataFetcherPlugin()->getResponseContent($url);

    // Convert objects to associative arrays.
    $source_data = json_decode($response, TRUE);

    // If json_decode() has returned NULL, it might be that the data isn't
    // valid utf8 - see http://php.net/manual/en/function.json-decode.php#86997.
    if (is_null($source_data)) {
      $utf8response = utf8_encode($response);
      $source_data = json_decode($utf8response, TRUE);
    }

    // Code to modify the source data as per Drupal needs.
    if (is_array($source_data)) {
      $source_data = $this->processSourceData($source_data);
    }

    // In migration config, we will use our new source instead of
    // original source from JSON.
    // For individual paragraphs source would be 'timezones' and
    // for main content where we import multiple values using
    // migration_loop, we will use timezone_ids inside data in fields
    // and id for iterator.
    $selectors = explode('/', trim($this->itemSelector, '/'));
    foreach ($selectors as $selector) {
      if (!empty($selector)) {
        $source_data = $source_data[$selector];
      }
    }

    return $source_data;
  }

  /**
   * Process source data.
   *
   * Add timezones as separate root element and timezones data with
   * proper ids in main content.
   *
   * @param array $source_data
   *   Source data.
   *
   * @return array
   *   Processed source data to add timezone_ids as separate root element and
   *   timezones data with proper ids in main content.
   */
  protected function processSourceData(array $source_data) : array {

    $source_data_new = [];

    foreach ($source_data as $row) {

      $id = $row['id'];

      foreach ($row['timezones'] as $delta => $data) {

        // Unique ID for each multi-value data item.
        $timezone_id = $id . '-' . $delta;

        // This will be used in migration of individual paragraphs.
        $data['timezoneId'] = $timezone_id;
        $row['timezones'][$delta] = $data;

        // This will be used in migration of main content.
        $row['timezone_ids'][] = $timezone_id;
      }

      // Converting Country Name Translations into key,value pair.
      foreach ($row['translations'] ?? [] as $key => $value) {
        $translations[$key] = ['key' => $key, 'value' => $value];
        $row['translations'] = $translations;
      }

      $source_data_new[] = $row;
    }

    return $source_data_new;

  }

}
